/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.ael.petdb.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author student
 */
@Entity
@Table(name = "human")

public class Owner {

    @Id
    @SequenceGenerator(name = "ownerSeq", sequenceName = "owner_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "ownerSeq")
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private String firstname;

    /**
     * Get the value of firstname
     *
     * @return the value of firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Set the value of firstname
     *
     * @param firstname new value of firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    private String secondname;

    /**
     * Get the value of secondname
     *
     * @return the value of secondname
     */
    public String getSecondname() {
        return secondname;
    }

    /**
     * Set the value of secondname
     *
     * @param secondname new value of secondname
     */
    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    private String thirdname;

    /**
     * Get the value of thirdname
     *
     * @return the value of thirdname
     */
    public String getThirdname() {
        return thirdname;
    }

    /**
     * Set the value of thirdname
     *
     * @param thirdname new value of thirdname
     */
    public void setThirdname(String thirdname) {
        this.thirdname = thirdname;
    }

}
