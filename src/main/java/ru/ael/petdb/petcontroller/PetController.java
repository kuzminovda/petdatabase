/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.ael.petdb.petcontroller;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import ru.ael.petdb.entity.Pet;
import ru.ael.petdb.petservice.PetService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author student
 */
@Path("/pets")
public class PetController {

    private static final Logger log = Logger.getLogger(PetController.class.getName());
    @Inject
    PetService pServ;

    @GET
    @Produces(MediaType.APPLICATION_JSON)   // Преобразует список в JSON массив
    public List<Pet> getAllPet() {
        log.log(Level.INFO, "Поступил запрос на получение полного списка животных");
        List<Pet> pets = pServ.getAll();
        if (pets != null) {
            log.log(Level.INFO, "Получен список животных [" + pets.size() + "] записей");
        }

        return pets;

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Pet create(Pet pet1) {
        log.log(Level.INFO, "Поступил запрос на сохранение животного в базе данных");
        Pet savedPet = pServ.savePet(pet1);
        if (savedPet != null) {
            log.log(Level.INFO, "Животное: [" + savedPet + "] сохранен в базе данных");
        }

        return savedPet;
    }

    /**
     * Метод удаления студента из базы данных
     *
     * @param student
     * @return
     */
    @Path("/{id}")
    public long delete(@PathParam("id") long id) {
        log.log(Level.INFO, "Поступил запрос на удаление животного из базы данных, идентификатор животного: [" + id + "]");
        long idDeletedStudent = pServ.delete(id);
        log.log(Level.INFO, "Животное c номером : [" + idDeletedStudent + "] удалено из базы данных");
        return idDeletedStudent;
    }

}
