/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.ael.petdb.petservice;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import ru.ael.petdb.entity.Pet;

/**
 *
 * @author student
 */
@ApplicationScoped
public class PetService {

    @Transactional
    public List<Pet> getAll() {

        List<Pet> pets = (List<Pet>) em.createQuery("Select t from " + Pet.class.getSimpleName() + " t").getResultList();

        return pets;
    }

    @Inject
    EntityManager em;

    @Transactional
    public Pet savePet(Pet pet1) {
        System.out.println("Сохранение в базе данных");
        em.persist(pet1);
        return (pet1);

    }

    @Transactional
    public long delete(long id) {
        em.remove(id);
        return id;
    }

}
