/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.ael.petdb.lifecycle;

import io.quarkus.runtime.StartupEvent;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import ru.ael.petdb.entity.Pet;
import ru.ael.petdb.petservice.PetService;

/**
 *
 * @author student
 */
@ApplicationScoped
public class LifeCycle {

    private static final Logger log = Logger.getLogger(LifeCycle.class.getName());

    /**
     * @Inject StudentService em;
     *
     * @Transactional void onStart(@Observes StartupEvent event) { log.info("["
     * + event.toString() + "] Сервер StudentMicroservice. Старт сервера ... ");
     * Student student = new Student(); student.setName("dima");
     * student.setMiddleName("tatarchuk"); em.saveStudent(student);
     *
     * log.info("[" + event.toString() + "] Сервер StudentMicroservice. Старт
     * сервера завершен."); }*
     */
    @Inject
    EntityManager em;
    @Inject
    PetService pServ;

    @Transactional
    public void onStart(@Observes StartupEvent event) {

        Pet pet1 = new Pet();
        pet1.setName("Мурзик");
        pServ.savePet(pet1);
        System.out.println("Сохранен pet1");

        Pet pet2 = new Pet();
        pet2.setName("Вася");
        pServ.savePet(pet2);
        System.out.println("Сохранен pet2");

        Pet pet3 = new Pet();
        pet3.setName("");
        pServ.savePet(pet3);
        System.out.println("Сохранен pet3");

    }
}
